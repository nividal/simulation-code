#ifndef PARAMETERS_H
#define PARAMETERS_H

#include<iostream>
#include<vector>
#include<string>
#include<sstream>
#include<fstream>
#include<cfloat>
/*
int scheduler_pol;
int controller_pol;
int sort_pol;
long double sensibility;
*/
class Parameters{
private:
  int n_inputs;
  int generator;
  std::vector<int> scheduler_policies;
  std::vector<int> controller_policies;
  std::vector<int> emulator_policies;
  std::vector<long double> alpha_tab;
  std::vector<long double> s_tab;
  std::vector<long double> b_tab;

  
  std::vector<int>::iterator scheduler_policy;
  std::vector<int>::iterator controller_policy;
  std::vector<int>::iterator emulator_policy;
  std::vector<long double>::iterator s;
  std::vector<long double>::iterator b;
  std::string indir;

  //std::ofstream cam;
  //std::ofstream cem;

public:
  Parameters();
  int getGen() const;
  int getInputs() const;
  std::vector<long double> getAlpha() const;
  std::string getDir() const;
  int nextConfig();

  int getSchedPol();
  int getContPol();
  int getEmPol();
  long double getS();
  long double getB();
  int nextParam();
  int printParam();

  int numParam();
  /*
  void Headers();
  std::ofstream getCam() const;
  std::ofstream getCem() const;
  void close_files();
  */

};


#endif
