import scipy.stats
import argparse
import random
import csv
import sys
import os
import string
from copy import deepcopy
from math import inf,sqrt
from path import Path

#####
## Apps as dictionaries
#####
# app={"id":string,"vol_w":int,"vol_io":int,"n_per":int,"chain":tab,"release":int,"it":int,"current_w":int,"current_io":int,seed:int,random_state,worked}

global time

###########################################################################################################################################################################
### Useful functions
###########################################################################################################################################################################


def lprint(s):
    print(s)
    with open("log.txt",'a') as l:
         l.write(s)
         l.write("\n")
    return


def partition(array, begin, end,order):
    pivot = begin
    for i in range(begin+1, end+1):
        if order(array[i], array[begin]):
            pivot += 1
            array[i], array[pivot] = array[pivot], array[i]
    array[pivot], array[begin] = array[begin], array[pivot]
    return pivot

def partition2(array,table,begin,end,order):
    pivot = begin
    for i in range(begin+1, end+1):
        if order(table[array[i]] , table[array[begin]]):
            pivot += 1
            array[i], array[pivot] = array[pivot],array[i]
    array[pivot],array[begin] = array[begin],array[pivot]
    return pivot


def qsort(l, table,order,begin=0, end=None):
    if end is None:
        end = len(l) - 1
    def _quicksort(l, table, begin, end):
        if begin >= end:
            return
        pivot = partition2(l,table, begin, end, order)
        _quicksort(l, table, begin, pivot-1)
        _quicksort(l, table, pivot+1, end)
    return _quicksort(l, table, begin, end)


###########################################################################################################################################################################
### Orders and comparators
###########################################################################################################################################################################

def fifo(app_1,app_2):
    return app_1["current_w"] < app_2["current_w"]


def johnson(app_1,app_2):
    if (app_1["vol_io"] >= app_1["vol_w"]):
        if (app_2["vol_io"] >= app_2["vol_w"]):
            return (app_1["vol_io"]==app_2["vol_io"] and app_1["vol_w"]<app_2["vol_w"]) or (app_1["vol_io"] > app_2["vol_io"])
        else:
            return True
    if (app_2["vol_io"] >= app_2["vol_w"]):
        return False
    return (app_1["vol_w"]==app_2["vol_w"] and app_1["vol_io"]>app_2["vol_io"]) or (app_1["vol_w"] < app_2["vol_w"])  

def remain(app_1,app_2):
    return (app_1["n_per"]-app_1["it"])*(app_1["vol_io"]*app_1["vol_w"]) <  (app_2["n_per"]-app_2["it"])*(app_2["vol_io"]*app_2["vol_w"])


#order used in list scheduling (sched) must check if apps are available 
def johnson_sched(app_1,app_2):
    if app_1["current_w"]*app_2["current_w"] > 0:
        return app_1["current_w"]<app_2["current_w"]
    if app_1["current_w"] <= 0 and app_2["current_w"] > 0:
        return True
    if app_1["current_w"] > 0 and app_2["current_w"] <= 0:
        return False
    if (app_1["vol_io"] >= app_1["vol_w"]):
        if (app_2["vol_io"] >= app_2["vol_w"]):
            return (app_1["vol_io"]==app_2["vol_io"] and app_1["vol_w"]<app_2["vol_w"]) or (app_1["vol_io"] > app_2["vol_io"])
        else:
            return True
    if (app_2["vol_io"] >= app_2["vol_w"]):
        return False
    return (app_1["vol_w"]==app_2["vol_w"] and app_1["vol_io"]>app_2["vol_io"]) or (app_1["vol_w"] < app_2["vol_w"])  

def remain_sched(app_1,app_2):
    if app_1["current_w"]*app_2["current_w"] > 0:
        return app_1["current_w"]<app_2["current_w"]
    if app_1["current_w"] <= 0 and app_2["current_w"] > 0:
        return True
    if app_1["current_w"] > 0 and app_2["current_w"] <= 0:
        return False
    return (app_1["n_per"]-app_1["it"])*(app_1["vol_io"]*app_1["vol_w"]) <  (app_2["n_per"]-app_2["it"])*(app_2["vol_io"]*app_2["vol_w"])



def chain_length(app_1,app_2):
    return app_1["n_per"] < app_2["n_per"]

def stretch(app,t):
    if t<=app["release"]:
        return inf
    return (app["worked_w"]+app["worked_io"])/(t-app["release"])


def min_stretch(app_1,app_2):
    if app_1["current_w"]*app_2["current_w"] > 0:
        return app_1["current_w"]<app_2["current_w"]

    if app_1["current_w"] <= 0 and app_2["current_w"] > 0:
        return True
    if app_1["current_w"] > 0 and app_2["current_w"] <= 0:
        return False
    global time
    if stretch(app_1,time)==-1:
        return False
    if stretch(app_2,time)==-1:
        return True
    return stretch(app_1,time)<stretch(app_2,time)





#### Not used yet
def rem_johnson(app_1,app_2):
    n_1 = app_1["n_per"] - app_1["it"]
    n_2 = app_2["n_per"] - app_2["it"]
    
    if (n_1*app_1["vol_io"] >= n_1*app_1["vol_w"]):
        if (n-2*app_2["vol_io"] >= n_2*app_2["vol_w"]):
            return (n_1*app_1["vol_io"]==n_2*app_2["vol_io"] and n_1*app_1["vol_w"]<n_2*app_2["vol_w"]) or (n_1*app_1["vol_io"] > n_2*app_2["vol_io"])
        else:
            return True
    if (n_2*app_2["vol_io"] >= n_2*app_2["vol_w"]):
        return False
    return (n_1*app_1["vol_w"]==n_2*app_2["vol_w"] and n_1*app_1["vol_io"]>n_2*app_2["vol_io"]) or (n_1*app_1["vol_w"] < n_2*app_2["vol_w"])  


def rem_johnson_sched(app_1,app_2):
    if app_1["current_w"]*app_2["current_w"] > 0:
        return app_1["current_w"]<app_2["current_w"]
    if app_1["current_w"] <= 0 and app_2["current_w"] > 0:
        return True
    if app_1["current_w"] > 0 and app_2["current_w"] <= 0:
        return False

    n_1 = app_1["n_per"] - app_1["it"]
    n_2 = app_2["n_per"] - app_2["it"]
    
    if (n_1*app_1["vol_io"] >= n_1*app_1["vol_w"]):
        if (n-2*app_2["vol_io"] >= n_2*app_2["vol_w"]):
            return (n_1*app_1["vol_io"]==n_2*app_2["vol_io"] and n_1*app_1["vol_w"]<n_2*app_2["vol_w"]) or (n_1*app_1["vol_io"] > n_2*app_2["vol_io"])
        else:
            return True
    if (n_2*app_2["vol_io"] >= n_2*app_2["vol_w"]):
        return False
    return (n_1*app_1["vol_w"]==n_2*app_2["vol_w"] and n_1*app_1["vol_io"]>n_2*app_2["vol_io"]) or (n_1*app_1["vol_w"] < n_2*app_2["vol_w"])  

## K johnson ?


###########################################################################################################################################################################
### Queue template
###########################################################################################################################################################################


class Queue(object):
    def __init__(self,app_list=[],order=fifo):
        self.queue = app_list
        self.order=order
        
    def __str__(self):
        return '['+', '.join([str(i) for i in self.queue]) + ']'
    
    def isEmpty(self):
        return len(self.queue) == 0

    def push(self,data):
        self.queue.append(data)

    def top(self):
        item=self.queue[0]
        del self.queue[0]
        return item
        
    def pop(self):
        try:
            top=0
            for i in range(len(self.queue)):
                if self.order(self.queue[i], self.queue[top]):
                    top = i
            item = self.queue[top]
            del self.queue[top]
            return item
        except IndexError:
            lprint("IndexError")
            exit()
        
    def sort(self, begin=0, end=None):
        if end is None:
            end = len(self.queue) - 1
        def _quicksort(begin, end):
            if begin >= end:
                return
            pivot = partition(self.queue, begin, end, self.order)
            _quicksort(begin, pivot-1)
            _quicksort(pivot+1, end)
        return _quicksort(begin, end)

    
    def map(fun,args):
        for i in range(len(self.queue)):
            self.queue[i]=fun(self.queue[i],args)
            
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
###########################################################################################################################################################################
### Algorithms / Heuristics
###########################################################################################################################################################################
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
            
###########################################################################################################################################################################
### List scheduling
###########################################################################################################################################################################

def sched(app_list,order,output,var=0,):
    fichier= open(output,"w")

    n=len(app_list)

    
    global time

    time=0

    waiting=[]
    running=[]

    makespan=0
    min_stretch=inf
    mean_stretch=0
    stretch=[]
    
    mass_io=0
    mass_w=0

    fi_di = 0
    fi_di_opt =0
    
    for app in app_list:
        app["worked_w"]=0
        app["worked_io"]=0
        app["it"]=1
        random.seed(app["seed"])
        app["current_w"]=app["vol_w"]*(1+random.uniform(-var,var))
        app["random_state"]=random.getstate()

        if app["release"]>0:
            waiting.append(app)
        else:
            running.append(app)
            if app["current_w"]:
                fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))
        fichier.write("")
    
    apps=Queue(running,order)
                        
    while not (apps.isEmpty() and len(waiting)==0):
        
        while (apps.isEmpty() and len(waiting)>0) :
            time+=1
            pt = 0
            #name_debug=''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)])
            while (pt < len(waiting)):
                app=waiting[pt]
                #print("{}      {}/{}    {}/{}\n ".format(name_debug,pt,len(waiting),time,app["release"]))
                if app["release"]<=time:
                    fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))
                    apps.push(app)
                    waiting.remove(app)
                else:
                    pt+=1

        pt = 0
        while (pt < len(waiting)):
            app=waiting[pt]
            if app["release"]<=time:
                fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))
                apps.push(app)
                waiting.remove(app)
            else:
                pt+=1

                        
        if (not apps.isEmpty()):

            ca=apps.pop()

            while (ca["current_w"] > 0):
                ca["current_w"]-=1
                ca["worked_w"]+=1
                fichier.write("{0} {1} {2} w:[{3} ; {4}]\n".format(time,"RUN_W",ca["id"],ca["current_w"]+1,ca["current_w"]))
                                   
                for app in apps.queue:
                    if app["current_w"]>0:
                        fichier.write("{0} {1} {2} w:[{3} ; {4}]\n".format(time,"RUN_W",app["id"],app['current_w'],app["current_w"]-1))
                        app["worked_w"]+=1
                    if app["current_w"]==0:
                        fichier.write("{0} {1} {2}\n".format(time,"STOP_W",app["id"]))
                    app["current_w"]-=1

                time+=1
                fichier.write("\n")

            if ca["current_w"]==0:
                fichier.write("{0} {1} {2}\n".format(time,"STOP_W",ca["id"]))
                
            random.setstate(ca["random_state"])
            current_io=ca["vol_io"]*(1+random.uniform(-var,var))
            ca["random_state"]=random.getstate()
                
            fichier.write("{0} {1} {2}\n".format(time,"START_IO",ca["id"]))
            
            while (current_io>0):
                fichier.write("{0} {1} {2} io:[{3} ; {4}]\n".format(time,"RUN_IO",ca["id"],current_io,current_io-1))
                current_io-=1
                ca["worked_io"]+=1

                pt = 0
                while (pt < len(waiting)):
                    app=waiting[pt]
                    if app["release"]<=time:
                        fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))
                        apps.push(app)
                        waiting.remove(app)
                    else:
                        pt+=1

                
                for app in apps.queue:
                    if app["current_w"]>0:
                        app["worked_w"]+=1
                        fichier.write("{0} {1} {2} w:[{3} ; {4}]\n".format(time,"RUN_W",app["id"],app["current_w"],app["current_w"]-1))
                    if app["current_w"]==0:
                        fichier.write("{0} {1} {2}\n".format(time,"STOP_W",app["id"]))
                    app["current_w"]-=1
            
                
                time+=1
                fichier.write("\n")
                        
            fichier.write("{0} {1} {2}\n".format(time,"STOP_IO",ca["id"]))
            ca["it"]+=1
            if ca["it"]<= ca["n_per"]:
                random.setstate(ca["random_state"])
                ca["current_w"]=ca["vol_w"]*(1+random.uniform(-var,var))
                ca["random_state"]=random.getstate()
                apps.push(ca)
                if ca["current_w"]>0:
                    fichier.write("{0} {1} {2}\n".format(time,"START_W",ca["id"]))
            else:
                fichier.write("{0} {1} {2}\n".format(time,"END_EXEC",ca["id"]))
                makespan=time        
                min_stretch=min(min_stretch, (ca["worked_w"]+ca["worked_io"])/(time-ca["release"]))
                stretch.append((ca["worked_w"]+ca["worked_io"])/(time-ca["release"]))
                mean_stretch+= ( (ca["worked_w"]+ca["worked_io"])/(time-ca["release"]))
                mass_io+=ca["worked_io"]
                mass_w+=ca["worked_w"]
                
                
                
                fi_di+= ca["worked_w"]/(time-ca["release"])
                fi_di_opt+= ca["worked_w"]/(ca["worked_io"]+ca["worked_w"])

    mean_stretch /= n

    fi_di /= n
    fi_di_opt /= n

    sd=0
    for s in stretch:
        sd += (s - mean_stretch)**2
    sd = sqrt(sd/n)

    
    fichier.write("MAKESPAN {0}\n".format(makespan))
    fichier.write("MIN STRETCH {0}\n".format(min_stretch))
    fichier.close()
    
    return "{0} \t  {1} \t {2} \t {3} \t {4} \t {5} \t {6} \t {7}".format(mass_io, mass_io + mass_w, makespan, min_stretch, mean_stretch, fi_di, fi_di_opt,sd)


###########################################################################################################################################################################
### Periodic (1 occurence/period)
###########################################################################################################################################################################
def per(list_apps,order,output,var=0):
    fichier= open(output,"w")
    global time
    time=0

    running=[]
    waiting=[]

    n=len(list_apps)

    
    makespan=0
    min_stretch=inf
    mean_stretch=0
    stretch=[]
    
    mass_io=0
    mass_w=0

    
    fi_di = 0
    fi_di_opt =0
    
    for app in list_apps:
        random.seed(app["seed"])
        app["current_w"]=app["vol_w"]*(1+random.uniform(-var,var))
        app["random_state"]=random.getstate()
        random.seed(app["seed"])
        app["worked_w"]=0
        app["worked_io"]=0
        app["it"]=1
        if app["release"]>0:
            waiting.append(app)
        else:
            running.append(app)
        
    apps=Queue(running,order)
    apps.sort()
    
    for app in apps.queue:
        if app["vol_w"]:
            fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))
    
    while not ( apps.isEmpty() and len(waiting)==0 ) :

        while(apps.isEmpty() and len(waiting)>0 ):
            time+=1

            pt = 0
            while (pt < len(waiting)):
                app=waiting[pt]
                if app["release"]<=time:
                    fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))
                    apps.push(app)
                    waiting.remove(app)
                else:
                    pt+=1
        
        pt = 0
        while (pt < len(waiting)):
            app=waiting[pt]
            if app["release"]<=time:
                fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))
                apps.push(app)
                waiting.remove(app)
            else:
                pt+=1

                
        ca=apps.top()

        while (ca["current_w"] > 0):
            ca["current_w"]-=1
            ca["worked_w"]+=1
            
            if ca["current_w"]>=0:
                fichier.write("{0} {1} {2} w:[{3} ; {4}]\n".format(time,"RUN_W",ca["id"],ca["current_w"]+1,ca['current_w']))
            if ca["current_w"]==0:
                fichier.write("{0} {1} {2}\n".format(time,"STOP_W",ca["id"]))
            for app in apps.queue:
                app["current_w"]-=1
                if app["current_w"]>=0:
                    fichier.write("{0} {1} {2} w:[{3} ; {4}]\n".format(time,"RUN_W",app["id"],app['current_w']+1,app['current_w']))
                    app["worked_w"]+=1
                if app["current_w"]==0:
                    fichier.write("{0} {1} {2}\n".format(time,"STOP_W",app["id"]))
                    
            time+=1
            fichier.write("\n")
            
        random.setstate(ca["random_state"])
        current_io=ca["vol_io"]*(1+random.uniform(-var,var))
        ca["random_state"]=random.getstate()

        if current_io>0:
            fichier.write("{0} {1} {2}\n".format(time,"START_IO",ca["id"]))
            
        while (current_io>0):
            fichier.write("{0} {1} {2} io:[{3} ; {4}]\n".format(time,"RUN_IO",ca["id"],current_io,current_io-1))
            current_io-=1
            ca["worked_io"]+=1

            pt = 0
            while (pt < len(waiting)):
                app=waiting[pt]
                if app["release"]<=time:
                    fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))
                    apps.push(app)
                    waiting.remove(app)
                else:
                    pt+=1

                        
            for app in apps.queue:
                app["current_w"]-=1
                                
                if app["current_w"]>0:
                    fichier.write("{0} {1} {2} w:[{3} ; {4}]\n".format(time,"RUN_W",app["id"],app["current_w"]+1,app["current_w"]))
                    app["worked_w"]+=1
                if app["current_w"]==0:
                    fichier.write("{0} {1} {2}\n".format(time,"STOP_W",app["id"]))
                    app["worked_w"]+=1
            time+=1
            fichier.write("\n")
        fichier.write("{0} {1} {2}\n".format(time,"STOP_IO",ca["id"]))
        
        
        if ca["it"]< ca["n_per"]:
            random.setstate(ca["random_state"])
            ca["current_w"]=ca["vol_w"]*(1+random.uniform(-var,var))
            ca["random_state"]=random.getstate()

            apps.push(ca)
            
            if ca["vol_w"]>0:
                fichier.write("{0} {1} {2}\n".format(time,"START_W",ca["id"]))
            ca["it"]+=1
        else:
            fichier.write("{0} {1} {2}\n".format(time,"END_EXEC",ca["id"]))
            makespan=time
            min_stretch=min(min_stretch, (ca["worked_w"]+ca["worked_io"])/(time-ca["release"]))
            stretch.append( (ca["worked_w"]+ca["worked_io"])/(time-ca["release"]))
            mean_stretch+= ((ca["worked_w"]+ca["worked_io"])/(time-ca["release"]))
            mass_io+=ca["worked_io"]
            mass_w+=ca["worked_w"]            
                
            fi_di+= ca["worked_w"]/(time-ca["release"])
            fi_di_opt+= ca["worked_w"]/(ca["worked_io"]+ca["worked_w"])               

    mean_stretch /= n
    fi_di /= n
    fi_di_opt /= n

    sd=0
    for s in stretch:
        sd += (s - mean_stretch)**2
    sd = sqrt(sd/n)
        
    fichier.write("MAKESPAN {0}\n".format(makespan))
    fichier.write("MIN STRETCH {0}\n".format(min_stretch))
    fichier.close()
    
    return "{0} \t  {1} \t {2} \t {3} \t {4} \t {5} \t {6} \t {7}".format(mass_io, mass_io + mass_w, makespan, min_stretch, mean_stretch, fi_di, fi_di_opt,sd)

###########################################################################################################################################################################
### Best effort
###########################################################################################################################################################################
def best_effort(list_app,output,var=0):
    fichier= open(output,"w")
    global time
    time=0;
    current_io=[]

    running=[]
    waiting=[]

    makespan=0
    min_stretch=inf
    mean_stretch=0
    stretch=[]

    cong=0
    
    n=len(list_app)

    mass_io=0
    mass_w=0

    
    fi_di = 0
    fi_di_opt =0
    
    
    for app in list_app:
        app["worked_w"]=0
        app["worked_io"]=0
        app["it"]=1
        if app["release"]:
            waiting.append(app)
        else:
            running.append(app)
        
        if app["vol_w"]:
            random.seed(app["seed"])
            app["current_w"]=app["vol_w"]*(1+random.uniform(-var,var))
            app["random_state"]=random.getstate()

            
    apps=Queue(running,fifo)

    for app in apps.queue:
        fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))
        
    while (not (apps.isEmpty() and waiting == [] ) or current_io != []):
        quantum = inf

        pt = 0
        while (pt < len(waiting)):
            app=waiting[pt]
            if app["release"]<=time:
                fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))
                if app["vol_w"]>0:
                    fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))
                apps.push(app)
                waiting.remove(app)
            else:
                quantum=min(quantum,app["release"]-time)
                pt+=1


        pt = 0
        while(pt < len(apps.queue)):
            app=apps.queue[pt]
            if app["current_w"]>0:
                quantum=min(quantum,app["current_w"])
                pt += 1
            else:
                fichier.write("{0} {1} {2}\n".format(time,"STOP_W",app["id"]))
                fichier.write("{0} {1} {2}\n".format(time,"START_IO",app["id"]))
                random.setstate(app["random_state"])
                app["current_io"]=app["vol_io"]*(1+random.uniform(-var,var))
                app["random_state"]=random.getstate()
                current_io.append(app)
                apps.queue.remove(app)

                   
        quantum_io=inf
        pt = 0
        
        while pt < len(current_io):
            app = current_io[pt]
            if app["current_io"]>0:
                quantum_io=min(quantum_io,app["current_io"])
                pt +=1
            else:
                current_io.remove(app)
                
                app["it"]+=1
                if app["it"]<=app["n_per"]:
                    fichier.write("{0} {1} {2}\n".format(time,"STOP_IO",app["id"]))
                    apps.push(app)
                    
                    if app["vol_w"]>0:
                        fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))

                        random.seed(app["seed"])
                        app["current_w"]=app["vol_w"]*(1+random.uniform(-var,var))
                        app["random_state"]=random.getstate()
                    if app["current_w"]>0:
                        quantum=min(quantum,app["current_w"])
                else:
                    fichier.write("{0} {1} {2}\n".format(time,"END_EXEC",app["id"]))
                    makespan=time        
                    min_stretch=min(min_stretch, (app["worked_w"]+app["worked_io"])/(time-app["release"]))
                    mean_stretch+= (app["worked_w"]+app["worked_io"])/(time-app["release"])
                    stretch.append( (app["worked_w"]+app["worked_io"])/(time-app["release"]))
                    mass_io+=app["worked_io"]
                    mass_w+=app["worked_w"]
                    
                    fi_di+= app["worked_w"]/(time-app["release"])
                    fi_di_opt+= app["worked_w"]/(app["worked_io"]+app["worked_w"])                     

        if quantum_io<inf:
            quantum=min(quantum,quantum_io*len(current_io))

        if quantum<inf:
            cong += len(current_io)*quantum

            fichier.write("# io concurrents : {}    and quantum {}\n".format(len(current_io),quantum))
                        
            for app in apps.queue:
                fichier.write("{0} {1} {2} w:[{3} ; {4} ] \n".format(time,"RUN_W",app["id"],app["current_w"],(app["current_w"]-quantum)))
                app["current_w"]-=quantum
                app["worked_w"]+=quantum
                
            for app in current_io:
                fichier.write("{0} {1} {2} io:[{3} ; {4} ]\n".format(time,"RUN_IO",app["id"],app["current_io"],app["current_io"]-quantum/len(current_io)))
                app["current_io"] -= (quantum/len(current_io))
                app["worked_io"] += (quantum/len(current_io))
        
            
            time+=quantum
        

    mean_stretch /= n

    fi_di /= n
    fi_di_opt /= n
    cong /= makespan
    sd=0
    for s in stretch:
        sd += (s - mean_stretch)**2
    sd = sqrt(sd/n)
    
    fichier.write("MAKESPAN {0}\n".format(makespan))
    fichier.write("MIN STRETCH {0}\n".format(min_stretch))
    fichier.close()
    return "{0} \t  {1} \t {2} \t {3} \t {4} \t {5} \t {6} \t {7} \t {8}".format(mass_io, mass_io + mass_w, makespan, min_stretch, mean_stretch, fi_di, fi_di_opt,sd,cong)

###########################################################################################################################################################################
### HRR
###########################################################################################################################################################################
# no release dates, not tested

def hrr(list_app,output,var=0):
    global time
    time=0
    
    fichier= open(output,"w")
    
    apps=Queue(list_app,chain_length)
    apps.sort()

    makespan=0
    stretch=inf

    for app in apps.queue:
        app["it"]=1

    a=apps.pop()
    sched=[]
    for i in range(a["n_per"]):
        sched.append([a])

    i=0
    while (not apps.isEmpty()):
        a=apps.pop()
        for it in range(a["n_per"]):
            sched[i].append(a)
            i+=1
            if i==len(sched()):
                i=0
            elif i==len(sched()-1) and a["nb_per"]<len(sched()-1):
                i=0

    #Sched contains a reversed schedule -> lprint:

    running=[]
    curr_io=None
        
    quanta=-1
    for app in list_apps:
        random.seed(app["seed"])
        app["current_w"]=app["vol_w"]*(1+random.uniform(-var,var))
        app["random_state"]=random.getstate()

        if app["current_w"]>0:
            fichier.write("{0} {1} {2}\n".format(time,"START_W",app["id"]))
        if quanta != -1:
            quanta=min(quanta,app["current_w"])
        else:
            quanta=app["current_w"]
        
        running.append(app)

        while not (len(running)==0 and curr_io == None):
            time+=quanta

            for app in running:
                if app["current_w"]>quanta:
                    fichier.write("{0} {1} {2}\n".format(time,"RUN_W",app["id"]))
                elif app["current_w"]==quanta:
                    fichier.write("{0} {1} {2}\n".format(time,"RUN_W",app["id"]))
                app["current_w"]-=quanta
                app["worked"]+=quanta

            if (curr_io != None):
                curr_io["current_io"] -= quanta
                curr_io["worked"]+=quanta
                if curr_io["current_io"]>0:
                    fichier.write("{0} {1} {2}\n".format(time,"RUN_W",curr_io["id"]))
                else:
                    fichier.write("{0} {1} {2}\n".format(time,"RUN_W",curr_io["id"]))

                    curr_io["it"]+=1
                    
                    if curr_io["it"] == curr_io["n_per"]:
                        fichier.write("{0} {1} {2}\n".format(time,"END_EXEC",curr_io["id"]))
                        lprint("{0} {1} {2}\n".format(time,"END_EXEC",curr_io["id"]))
                        makespan=time
                        stretch=min(stretch, curr_io["worked"]/(time-curr_io["release"]))
                       
                    if curr_io["it"]<curr_io["n_per"]:
                        random.setstate(curr_io["random_state"])
                        curr_io["current_w"]=curr_io["vol_w"]*(1+random.uniform(-var,var))
                        curr_io["random_state"]=random.getstate()

                        running.append(curr_io)
                    curr_io=None

                   
            if (curr_io == None):
                for app in running:
                    if app["current_w"]<=0 and app["id"] == sched[-1]["id"]:

                        random.setstate(app["random_state"])
                        app["current_io"]=app["vol_io"]*(1+random.uniform(-var,var))
                        app["random_state"]=random.getstate()

                        curr_io=app
                        fichier.write("{0} {1} {2}\n".format(time,"START_IO",curr_io["id"]))

                        running.remove(app)
                        del sched[-1]
                        
    fichier.write("MAKESPAN {0}\n".format(makespan))
    fichier.write("MIN STRETCH {0}\n".format(stretch))
    fichier.close()
    return "{0}    {1}".format(makespan,stretch)
    
"""
def brut(list_app,output):
    fichier=open(output,"w")
    '''"Complicated'''
    ## Naive list generation
    ## forall permutation evaluate criterium
    ## keep optimal
    fichier.close()
    return
"""


###########################################################################################################################################################################
### Iterative period
###########################################################################################################################################################################
### Code not adapted for variation yet


#Period costs
#As late as possible
def cost(period,sio,table):
    #sio=[]
    timestamps=[]
    #dic = {}
    nextio=0
    makespan=0

    for a in period:
        app=table[a]
        app["next_t"]=0
        app["firstw"]=-1
        
    for a in period:
        app=table[a]
        app["next_t"]=max(nextio+app["vol_io"],app["next_t"]+app["vol_w"]+app["vol_io"])
        if app["firstw"]==-1:
            app["first_w"]=app["next_t"]-app["vol_io"]-app["vol_w"]
        nextio=app["next"]
        makespan=max(makespan,app["next_t"])
        sio.append(nextio-app["vol_io"])
        
    firstio=table[period[0]]['vol_w']
    
    overlap=inf
    for a in period:
        app=table[a]
        overlap=min(overlap, makespan-app["next_t"]+firstio, makespan-app["next_t"]+app["firstw"])

    return (makespan-overlap)

def ins_asap(app,period,sio,table):
    asap=app["next"]

    i=0
    while(i<len(sio)):
        if sio[i]>asap:
            period.insert(i,app["id"])
            return
        i+=1
    period.append(app["id"])
    return

### Timestamps of optimal period/ handles period overlapping
#printing and updating metrics
def stamps(period,nextio,fichier,table):
    global time
    for a in period:
        app=table[a]
        app["it"]+=1
        if nextio+app["vol_io"]>app["next"]+app["vol_w"]+app["vol_io"]:
            fichier.write("[{0} ; {1}] {2} {3}\n".format(nextio-app["vol_w"],nextio,"RUN_W",app["id"]))
            fichier.write("[{0} ; {1}] {2} {3}\n".format(nextio,nextio+app["vol_io"],"RUN_IO",app["id"]))
        else:
            fichier.write("[{0} ; {1}] {2} {3}\n".format(app["next"],app["next"]+app["vol_w"],"RUN_W",app["id"]))
            fichier.write("[{0} ; {1}] {2} {3}\n".format(app["next"]+app["vol_w"], app["next"]+app["vol_w"]+app["vol_io"] ,"RUN_IO", app["id"] ))
        app["next"]=max(nextio+app["vol_io"],app["next"]+app["vol_w"]+app["vol_io"])
        nextio=app["next"]
        #makespan=max(makespan,app["next"])
        app["worked_w"]+=app["vol_w"]
        app["worked_io"]+=app["vol_io"]
        
        

def iterative_period(list_apps,output,var=0):


    table={}
    for app in list_apps:
        table[app["id"]]=app

    
    fichier= open(output,"w")
    n=len(list_apps)
    running=[]
    waiting=[]
    nextio=0

    for app in list_apps:
        random.seed(app["seed"])
        app["current_w"]=app["vol_w"]*(1+random.uniform(-var,var))
        app["random_state"]=random.getstate()
        random.seed(app["seed"])
        app["worked_w"]=0
        app["worked_io"]=0
        app["it"]=1
        if app["release"]>0:
            waiting.append(app["id"])
        else:
            running.append(app["id"])
        app["next"]=0
            
    #global time
    #time = 0

    
    #Initialization
    makespan_final=0       
    min_stretch_final=inf
    mean_stretch=0
    mass_io=0
    mass_w=0
    stretch=[]
    
    fi_di = 0
    fi_di_opt = 0
    
    
    while not (len(waiting)==0 and len(running)==0):
        temp_l=[]
        k_limit=0
        for a in running:
            if not a in temp_l:
                table[a]["ki"]=1
                table[a]["ki_opt"]=1
                k_limit+=(table[a]["n_per"]-table[a]["it"]+1)
                temp_l.append(a)
        period=temp_l
            
        sk=len(running)
        min_stretch=inf
        
        #ŧrié par ordre d'IO (et par début de calcul)
        period=deepcopy(running)
        
        qsort(period,table,johnson)
        #dic={}
        opt_per=[]
        opt_cper=0

                
        while( True ):
            temp_stretch = inf

            sio=[]
            cper=cost(period,sio,table)
                
            for a in running:
                app=table[a]
                app["temp_stretch"]= (app["ki"]*(app["vol_io"]+app["vol_w"]))/ cper 
                temp_stretch=min(temp_stretch, app["temp_stretch"])
                    
            if temp_stretch < min_stretch:
                for a in running:
                    app=table[a]
                    app["ki_opt"]=app["ki"]
                min_stretch=temp_stretch
                opt_per=period
                opt_cper=cper

            sk+=1
            
            if sk >= k_limit:
                break
            else:
                min_stretch_improvable=inf
                app_improvable=None
                for a in running:
                    app=table[a]
                    if (app["temp_stretch"]<min_stretch_improvable) and (app["ki"] < (app["n_per"] - app["it"]+1)):
                        min_stretch_improvable=app["temp_stretch"]
                        app_improvable=app
                app_improvable["ki"]+=1
                ins_asap(app_improvable,period,sio,table)
                    
        
        
        stable=True

        while(stable):
            stamps(opt_per,nextio,fichier,table)
            #time+=opt_cper

            i = 0
            while (i<len(running)):
                app=table[running[i]]
                
                if app["it"]>app["n_per"]:
                    fichier.write("{0} {1} {2}\n".format(app["next"], "END_EXEC", app["id"]))
                    #affichage fin d'execution et mesure des métriques makespan = app["next"]
                    makespan_final=max(makespan_final,app["next"])
                    
                    min_stretch_final=min(min_stretch_final, (app["worked_w"]+app["worked_io"])/(app["next"]-app["release"]))
                    mean_stretch+= (app["worked_w"]+app["worked_io"])/(app["next"]-app["release"])
                    mass_io+=app["worked_io"]
                    mass_w+=app["worked_w"]
                    stretch.append(min_stretch_final)
                    
                    fi_di+= app["worked_w"]/(app["next"]-app["release"])
                    fi_di_opt+= app["worked_w"]/(app["worked_io"]+app["worked_w"])
                    
                    running.remove(app["id"])
                    
                    stable=False
                elif (app["n_per"]-app["it"]+1<app['ki_opt']):
                    stable=False
                    i+=1
                else:
                    i+=1

            i=0
            while (i<len(waiting)):
                app=table[waiting[i]]
                if app["release"]<=time:
                    stable=False
                    running.append(app["id"])
                    waiting.remove(app["id"])
                    fichier.write("{0} {1} {2}\n".format(app["release"], "RELEASE", app["id"]))
                else:
                    i+=1

            """ remove multiple occurences"""
            
            fichier.write("\n")
    
    
    mean_stretch /= n

    fi_di /= n
    fi_di_opt /= n
    sd=0
    for s in stretch:
        sd += (s - mean_stretch)**2
    sd = sqrt(sd/n)


    fichier.close()
    
    return "{0} \t  {1} \t {2} \t {3} \t {4} \t {5} \t {6} \t {7}".format(mass_io, mass_io + mass_w, makespan_final, min_stretch_final, mean_stretch, fi_di, fi_di_opt,sd)
    




"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
###########################################################################################################################################################################
### Main fuction / experiments script
###########################################################################################################################################################################
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if __name__ == '__main__':
    random.seed()

    # app={"id":string,"vol_w":int,"vol_io":int,"n_per":int,"chain":tab,"release":int,"it":int,"current_w":int,"current_io":int,rand_seed:int,random_state}
    
    """
    app1={"id":"app1","vol_w":3,"vol_io":2,"n_per":3}
    app2={"id":"app2","vol_w":2,"vol_io":3,"n_per":3}
    app3={"id":"app3","vol_w":3,"vol_io":2,"n_per":2}
    app4={"id":"app4","vol_w":2,"vol_io":2,"n_per":2}
    """
    
     
    if not os.path.exists('out'):
        os.makedirs('out')



    ###################################################
    ### Results header
    ###################################################
    with open("out/results_sched_fifo.csv","a") as res:
        res.write("name \t ex_mass_io \t mass_io \t mass_io+mass_w \t  makespan \t  min_stretch \t mean_stretch \t Fi_di \t Fi_diopt \t sd \n")
            
    with open("out/results_per_fifo.csv","a") as res:
        res.write("name \t ex_mass_io \t mass_io \t mass_io+mass_w \t  makespan \t  min_stretch \t mean_stretch \t  Fi_di \t Fi_diopt \t sd \n")
        
    with open("out/results_sched_johnson.csv","a") as res:
        res.write("name \t ex_mass_io \t mass_io \t mass_io+mass_w \t  makespan \t  min_stretch \t mean_stretch \t  Fi_di \t Fi_diopt \t sd \n")
                
    with open("out/results_per_johnson.csv","a") as res:
        res.write("name \t ex_mass_io \t mass_io \t mass_io+mass_w \t  makespan \t  min_stretch \t mean_stretch \t  Fi_di \t Fi_diopt \t sd \n")

    with open("out/results_sched_stretch.csv","a") as res:
        res.write("name \t ex_mass_io \t mass_io \t mass_io+mass_w \t  makespan \t  min_stretch \t mean_stretch \t  Fi_di \t Fi_diopt \t sd \n")
            
    with open("out/results_best_effort.csv","a") as res:
        res.write("name \t ex_mass_io \t mass_io \t mass_io+mass_w \t  makespan \t  min_stretch \t mean_stretch \t  Fi_di \t Fi_diopt \t sd \t cong \n")

    with open("out/results_sched_remain.csv","a") as res:
        res.write("name \t ex_mass_io \t mass_io \t mass_io+mass_w \t  makespan \t  min_stretch \t mean_stretch \t  Fi_di \t Fi_diopt \t sd \n")
    with open("out/results_per_remain.csv","a") as res:
        res.write("name \t ex_mass_io \t mass_io \t mass_io+mass_w \t  makespan \t  min_stretch \t mean_stretch \t  Fi_di \t Fi_diopt \t sd \n")

    with open("out/results_iterative_period.csv","a") as res:
        res.write("name \t ex_mass_io \t mass_io \t mass_io+mass_w \t  makespan \t  min_stretch \t mean_stretch \t  Fi_di \t Fi_diopt \t sd \n")

    
    
    nb_input=0
    for input_file in Path('in/').walkfiles():
        nb_input+=1
    
    cpt_file=0
    for input_file in Path('in/').walkfiles():
        applist=[]
        cpt_file+=1


        out='.'.join(input_file.split('.')[:-1]) #suppression de l'extension
        out="out/{}".format(out)
        if not os.path.exists(out):
            os.makedirs(out)

        name_file='.'.join(input_file.split('.')[:-1]).split('/')[1]

        
        ##############
        ## Progression prompt
        ##############
        sys.stdout.write("\rfile {0} out of {1} - ({2}%)".format(cpt_file,nb_input,int((cpt_file/nb_input)*100)))

        with open(input_file,"r") as f:
            for line in f.readlines():
               applist.append(eval(line))

        mass_io=0
        #####
        ### Additional infos
        ####
        for app in applist:
            app["seed"]=random.random()
            mass_io+= int(app["vol_io"])*int(app["n_per"])




        ###############################################
        ## Traces/output generation
        ###############################################
        var=0
        #lprint("sched fifo")
        if not os.path.isfile("{0}/sched_fifo.txt".format(out)):
            r=sched(deepcopy(applist),fifo,"{0}/sched_fifo.txt".format(out),var)
            with open("out/results_sched_fifo.csv","a") as res:
                res.write("{0} \t {1} \t {2}\n".format(name_file,mass_io,r))
            
        #lprint("per fifo")
        if not os.path.isfile("{0}/per_fifo.txt".format(out)):      
            r=per(deepcopy(applist),fifo,"{0}/per_fifo.txt".format(out),var)
            with open("out/results_per_fifo.csv","a") as res:
                res.write("{0} \t {1} \t {2}\n".format(name_file,mass_io,r))
        #lprint("sched johnson")
        
        if not os.path.isfile("{0}/sched_johnson.txt".format(out)):
            r=sched(deepcopy(applist),johnson_sched,"{0}/sched_johnson.txt".format(out),var)
            with open("out/results_sched_johnson.csv","a") as res:
                res.write("{0} \t {1} \t {2}\n".format(name_file,mass_io,r))
                
        #lprint("per johnson")
        if not os.path.isfile("{0}/per_johnson.txt".format(out)):
            r=per(deepcopy(applist),johnson,"{0}/per_johnson.txt".format(out),var)
            with open("out/results_per_johnson.csv","a") as res:
                res.write("{0} \t {1} \t {2}\n".format(name_file,mass_io,r))

        #lprint("sched stretch")
        if not os.path.isfile("{0}/sched_stretch.txt".format(out)):
            r=sched(deepcopy(applist),min_stretch,"{0}/sched_stretch.txt".format(out),var)
            with open("out/results_sched_stretch.csv","a") as res:
                res.write("{0} \t {1} \t {2}\n".format(name_file,mass_io,r))
            
        #lprint("best effort")
        if not os.path.isfile("{0}/best_effort.txt".format(out)):
            r=best_effort(deepcopy(applist),"{0}/best_effort.txt".format(out),var)
            with open("out/results_best_effort.csv","a") as res:
                res.write("{0} \t {1} \t {2}\n".format(name_file,mass_io,r))

        
        if not os.path.isfile("{0}/sched_remain.txt".format(out)):
            r=sched(deepcopy(applist),remain_sched,"{0}/sched_remain.txt".format(out),var)
            with open("out/results_sched_remain.csv","a") as res:
                res.write("{0} \t {1} \t {2}\n".format(name_file,mass_io,r))

        if not os.path.isfile("{0}/per_remain.txt".format(out)):
            r=per(deepcopy(applist),remain,"{0}/per_remain.txt".format(out),var)
            with open("out/results_per_remain.csv","a") as res:
                res.write("{0} \t {1} \t {2}\n".format(name_file,mass_io,r))


        if not os.path.isfile("{0}/iterative_period.txt".format(out)):
            r=iterative_period(deepcopy(applist),"{0}/iterative_period.txt".format(out),var)
            with open("out/results_iterative_period.csv","a") as res:
                res.write("{0} \t {1} \t {2}\n".format(name_file,mass_io,r))

