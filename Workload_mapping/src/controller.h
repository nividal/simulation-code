#ifndef CONTROLLER_H
#define CONTROLLER_H

#include<vector>
#include "App.h"
#include "Platform.h"
#include "emulator.h"
#include "Parameters.h"

void controller(std::vector<App*> *applist, Parameters *param,std::ofstream& cam, std::ofstream& cem, std::ofstream& cocc);

#endif
