#include "controller.h"


void controller(std::vector<App*> *applist,Parameters *param,std::ofstream& cam, std::ofstream& cem,  std::ofstream& cocc){

  Platform platform=Platform(applist, param);

  platform.print_status();
  
  long double next_event;
  int test;
  while ( !platform.isOver() )
    {
      std::cerr<<"place\n";
      platform.place(param);
      //platform.print_status();
      std::cerr<<test++<<std::endl;
      std::cerr<<"next\n";
      next_event=platform.next(param);
      std::cerr<<"resolve\n";
      resolve(&platform,param,next_event);
      std::cerr<<"incre\n";
      platform.incrClock(next_event);		   
      if (platform.getPocc()){
	platform.print_occupation(param,cocc);
	platform.unsetPocc();
      }
      
    }
      std::cerr<<"Execution completed ; metrics:\n";
      platform.print_metrics(param, cam, cem);
}
