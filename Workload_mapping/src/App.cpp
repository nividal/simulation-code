#include "App.h"


long double mod(long double a,long double b){
  int q=a/b;
 double qd=q;
 if (round((a-qd*b)*100000)/100000<0)
 	qd--;
  if (round((a-qd*b)*100000)/100000>b)
	qd++;
  return round((a-qd*b)*100000)/100000;
}


//Constructor
App::App(std::vector<std::string> param,int id,long double b){
  this->id=id;
  this->apptype=param[0];
  this->w=atof(param[2].c_str());
  this->p=atoi(param[3].c_str());
  this->v=atof(param[5].c_str());
  this->n=atoi(param[6].c_str());
  this->b=b;
  this->eta=(this->w+this->v/b)*this->n;
  
  //Undefined release date
  this->release=0;
  this->adv=0;
  this->active_io=false;
  this->prec.clear();

}

App::App(int id,  long double w,  int p,  int n, long double v){
  this->id=id;
  this->apptype="default";
  this->w=w;
  this->p=p;
  this->v=v;
  this->n=n;
  this->b=1; //DEFAULT ; be careful
  this->eta=(this->w+(this->v/this->b))*this->n;
  
  //Undefined release date
  this->release=0;
  this->adv=0;
  this->active_io=false;
  this->prec.clear();
}


//Setters & Getters 
int App::getId() const { return this->id; }
void App::setId(int id) {this->id=id;}
  
std::string App::getType() const {return this->apptype; }
void App::setType(std::string type) {this->apptype=type; }

long double App::getW() const {return this->w;}
void App::setW( long double w) {this->w=w;}

long double App::getB() const {return this->b;}

int App::getP() const {return this->p;}
void App::setP( int p) {this->p=p;}

long double App::getV() const {return this->v;}
void App::setV( long double v) { this->v=v;}

int App::getN() const {return this->n;}
void App::setN( int p) {this->n=n;}

long double App::getR() const {return this->release;}
void App::setR( long double release) {this->release=release;}

std::vector<App*> App::getPrec() const {return this->prec;}
void App::setPrec(std::vector<App*> prec) {this->prec=prec;}
void App::AddPrec(App* app) {this->prec.push_back(app);}
void App::RmPrec(App* app) {
  auto it=this->prec.begin();
  while (it != this->prec.end() )
    {
      if ( (*it)->getId() == app->getId())
	it=this->prec.erase(it);
      else
	it++;
    }
}
void App::cleanPrec(){this->prec.clear();}


long double App::getEta() const {return this->eta;}
void App::setEta( int eta) {this->eta=eta;}

int App::getIt() const {return this->adv/(this->w+this->v);}

long double App::getAdv() const {return this->adv;}
void App::incrAdv( long double t) {this->adv+=t;}


long double App::getAlpha() const {return (this->v/(this->w+this->v)) ;}

bool App::getIOstatus() const {return this->active_io;}
void App::setIO() {this->active_io=true;}
void App::unsetIO() {this->active_io=false;}

long double App::getStart() const {return this->starting_date;}
void App::setStart(long double s_d) {this->starting_date=s_d;}

long double App::getEnd() const {return this->end_date;}
void App::setEnd(long double e_d) {this->end_date=e_d;} 

long double App::next() {
  long double op= mod(this->adv , (this->w+this->v));
  //std::cout<<"adv:"<<this->adv<<"  op:"<<op<<" w="<<this->w<<" +v="<<this->v<<" next ";

  if (this->active_io)
    {
      //std::cout<<"en io:" <<(this->w+this->v-op)/this->b<<"   set?"<<this->active_io<<std::endl;
  	return round(((this->w+this->v-op)/this->b)*100000)/100000;  
  }
    
  else{
   	if (this->w-op<0)
	  op-=(this->w+this->v);
  	//std::cout<<"en calcul:"<<this->w-op<<" set?"<<this->active_io<<std::endl;
  	return round((this->w-op)*100000)/100000;
  }  
}




/*
Tools
 */

//Basic print ; to adapt according to apptype
void printApp(App *A,bool prec=true){
  std::cout<<"ID: "<<A->getId()<<" | Type: "<<A->getType()<<" | w: "<<A->getW()<<" | p: "<<A->getP()<<" | v: "<<A->getV()<<" | n: "<<A->getN()<<"\n";
  if (prec){
    std::cout<<"Precedence:"<<std::endl;
    std::vector<App*> lprec = A->getPrec(); //local copy
    for (std::vector<App*>::iterator jt= lprec.begin();jt!=lprec.end();jt++){
      std::cout<<"   ";
      printApp(*jt,false);
    }  
    std::cout<<std::endl;
  }
  
}

void print_applist(std::vector<App*> const &list){
  std::cout<<"Applications:\n";
  for (auto it = list.begin(); it != list.end(); ++it)
    {
      printApp(*it,false);	
    }
  std::cout<<"--\n";
}

bool CompRel(App* A1, App* A2){  
  return A1->getR() < A2->getR(); 
} 

bool CompTime(App* A1, App* A2){
  return A1->getEta() > A2->getEta(); 
} 

bool CompMax(App* A1, App* A2) {
  return std::max((long double)(A1->getP())/P,A1->getAlpha()/A1->getB()) < std::max((long double)(A2->getP())/P,A2->getAlpha()/A2->getB());
}

/*
bool CompArea(App* A1, App* A2) {
  if (A1->getP()*A1->getAlpha() == A2->getP()*A2->getAlpha())
    return std::max((long double)(A1->getP())/P,A1->getAlpha()/B) < std::max((long double)(A2->getP())/P,A2->getAlpha()/B);
  return (A1->getP()*A1->getAlpha() < A2->getP()*A2->getAlpha());
} 
bool CompVol(App* A1, App* A2)  {
  if (A1->getP()*A1->getAlpha()*A1->getEta()==A2->getP()*A2->getAlpha()*A2->getEta()){
    if (A1->getP()*A1->getAlpha() == A2->getP()*A2->getAlpha())
      return std::max((long double)(A1->getP())/P,A1->getAlpha()/B) < std::max((long double)(A2->getP())/P,A2->getAlpha()/B);
    return (A1->getP()*A1->getAlpha() < A2->getP()*A2->getAlpha());
    }
  return (A1->getP()*A1->getAlpha()*A1->getEta()<A2->getP()*A2->getAlpha()*A2->getEta());
}
*/


/*
Test with a different tchar -> avant on prennait seulement W
 */
bool CompTChar(App* A1, App* A2)  {
  return (A1->getW()+A1->getV()/A1->getB())>(A2->getW()+A2->getV()/A2->getB());
}



