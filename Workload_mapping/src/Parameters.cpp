#include "Parameters.h"
/*
int scheduler_pol;
int controller_pol;
int sort_pol;
long double sensibility;
int sort_pol;
*/

Parameters::Parameters(){
  this->scheduler_policies.clear();
  this->controller_policies.clear();
  this->emulator_policies.clear();
  this->alpha_tab.clear();
  this->s_tab.clear();
  this->b_tab.clear();
  //default
  n_inputs=0;
  generator=0;

  

  
  std::string str;
  std::string name;
  int choice;
  long double values;
  std::string keyword;
  while (getline(std::cin,str) ){
    std::istringstream iss(str);
    iss>>keyword;
    
    if (keyword=="n_inputs")
      {
	iss>>this->n_inputs;
      }
    if (keyword == "generator")
      {
	iss>>this->generator;
      }
    if (keyword=="alpha")
      {
	while (iss>>values)
	  this->alpha_tab.push_back(values);
      }
    if (keyword=="scheduler")
      {
	while (iss>>choice)
	  this->scheduler_policies.push_back(choice);
      }
    if (keyword=="controller")
      {
	while (iss>>choice)
	  this->controller_policies.push_back(choice);
      }
    if (keyword=="emulator")
      {
	while (iss>>choice)
	  this->emulator_policies.push_back(choice);
      }
    if (keyword=="S")
      {
	while (iss>>values)
	  this->s_tab.push_back(values);
      }
    if (keyword=="B")
      {
	while (iss>>values)
	  this->b_tab.push_back(values);	
      }
    if (keyword=="folder")
      {
	while (iss>>name)
	  this->indir=name;
      }
  }

  //Default;
  if ( this->scheduler_policies.empty() )
    this->scheduler_policies.push_back(0);
  
  if ( this->controller_policies.empty() )
    this->controller_policies.push_back(0);
  
  if (    this->emulator_policies.empty() )
    this->emulator_policies.push_back(0);
  
  if (    this->alpha_tab.empty() )
    this->alpha_tab.push_back(1);
  
  if (    this->s_tab.empty() )
    this->s_tab.push_back(1);   

  if (this->b_tab.empty() )
    this->b_tab.push_back(1);
  
  
    //verification
  /*
  std::cout<<"Parameters are: \n";
  std::cout<<"n_inputs: "<<this->n_inputs<<std::endl;
  std::cout<<"generator: "<<this->generator<<std::endl;
  std::cout<<"indir: "<<this->indir<<std::endl;
    
    std::cout<<"scheduler_policies: ";
    for (auto sp=this->scheduler_policies.begin();sp!=this->scheduler_policies.end();sp++)
      std::cout<<*sp<<" ";
    std::cout<<std::endl;
    
    std::cout<<"controller_policies: ";
    for (auto cp=this->controller_policies.begin();cp!=this->controller_policies.end();cp++)
      std::cout<<*cp<<" ";
    std::cout<<std::endl;
    
    std::cout<<"emulator_policies: ";
    for (auto ep=this->emulator_policies.begin();ep!=this->emulator_policies.end();ep++)
      std::cout<<*ep<<" ";
    std::cout<<std::endl;
    
    std::cout<<"alpha: ";
    for (auto at=this->alpha_tab.begin();at!=this->alpha_tab.end();at++)
      std::cout<<*at<<" ";
    
    std::cout<<std::endl;
    std::cout<<"S: ";
    for (auto st=this->s_tab.begin();st!=this->s_tab.end();st++)
      std::cout<<*st<<" ";
    std::cout<<std::endl;
  */
    
    this->scheduler_policy=scheduler_policies.begin();
    this->controller_policy=controller_policies.begin();
    this->emulator_policy=emulator_policies.begin();
    this->s=s_tab.begin();
    this->b=b_tab.begin();
    
}

int Parameters::getGen() const {return this->generator;}
int Parameters::getInputs() const {return this->n_inputs;}
std::vector<long double> Parameters::getAlpha() const {return this->alpha_tab;}
std::string Parameters::getDir() const {return this->indir;}


int Parameters::getSchedPol(){return *(this->scheduler_policy);}
int Parameters::getContPol() {return *(this->controller_policy);}
int Parameters::getEmPol() {return *(this->emulator_policy);}
long double Parameters::getS() {
  if ((*this->s)<0)
    return DBL_MAX;
  else
    return *(this->s);
}

long double Parameters::getB() {return *(this->b);}


int Parameters::nextParam(){
  this->b++;

  if (this->b == this->b_tab.end()){
    this->b=this->b_tab.begin();
    this->s++;
  }
  if (this->s == this->s_tab.end()){
    this->s=this->s_tab.begin();
    this->emulator_policy++;
  }
  if (this->emulator_policy == this->emulator_policies.end()){
    this->emulator_policy=this->emulator_policies.begin();
    this->controller_policy++;
  }
  if (this->controller_policy == this->controller_policies.end()){
    this->controller_policy=this->controller_policies.begin();
    this->scheduler_policy++;
  }
  
  if (this->scheduler_policy==this->scheduler_policies.end()){
    this->scheduler_policy=this->scheduler_policies.begin();
    return 0;
  }
  return 1;
}


int Parameters::printParam(){
  std::cout<<(*(this->scheduler_policy))<<"\t"<<(*(this->controller_policy))<<"\t"<<(*(this->emulator_policy))<<"\t";
  std::cout<<(*(this->s))<<"\t"<<(*(this->b))<<std::endl;
  return 0;
}

int Parameters::numParam(){
  return (this->controller_policies.size() * this->emulator_policies.size() * this->scheduler_policies.size() * this->n_inputs * this->alpha_tab.size() * this->s_tab.size());
}


/*

void Parameters::Headers(){  
  //Headers

  this->cam.open ("application_metrics.csv", std::ofstream::out | std::ofstream::app);
  this->cem.open ("execution_metrics.csv", std::ofstream::out | std::ofstream::app);
  
  
  this->cam<<"SchedPol \t ContPol \t Em \t S \t alpha \t p \t w \t v \t n \t start \t end"<<std::endl;
  this->cem<<"SchedPol \t ContPol \t Em \t S \t alpha \t makespan \t max_stretch \t mean_stretch"<<std::endl;
  
}
std::ofstream Parameters::getCam() const{
  return this->cam;
}
std::ofstream Parameters::getCem() const{
  return this->cem;
}

void Parameters::close_files(){
  this->cam.close();
  this->cem.close();
}

*/
