#ifndef GENERATOR_H
#define GENERATOR_H

#include<iostream>
#include<random>
#include<time.h>
#include<fstream>
#include<iomanip>
#include <cmath>        // std::std::abs

#include "Parameters.h"
#include "App.h"


#ifndef P
#define P 49152.0 //Mira
//#define P 2048 
#endif

#ifndef B
#define B 1.0
#endif




/*
const long double PI = 3.141592653589793238463;
long double l[12];


//---------------------------------------------------------------
  //set up random generators
std::random_device rd{};
std::mt19937 gen{rd()};

std::default_random_engine gen_random;
std::uniform_real_distribution<long double> distribution(0.0,1.0);
*/

//---------------------------------------------------------------
//Tools for generation according to alpha


//# Truncated normal tools
long double truncatedNormal(long double mean, long double sigma, long double lower, long double upper);
long double density(long double x);
long double cumulative(long double x);

//Mean of a truncated normal(mu,sigma) |  a<X<b
long double mean_trunc(long double mu,long double sigma, long double a, long double b);


//# Intermediate functions
int rec_li(int low,int high,long double tgt);
int compute_li(long double alpha,int p, long double mu1, long double mu2,long double mean_p);
int generate_pi();

//# Input generation functions
std::vector<App*> default_gen();
std::vector<App*> mira_gen();
std::vector<App*> alpha_gen(long double alpha,long double b);


// # Verification
long double verif_alpha(std::vector<App*> applist);


int generator(Parameters param);

#endif
