#include<iostream>
#include<fstream>
#include<iomanip>
#include<vector>
#include<sstream>
#include<memory>
#include<sys/types.h>
#include<dirent.h>
#include<chrono>
#include<ctime> 
#include<sys/stat.h>
#include<filesystem>
#include "App.h"
#include "Parameters.h"
#include "generator.h"
#include "controller.h"
#include "scheduler.h"
#include "Platform.h"


//#define P 49152

//#define P 2048

inline bool exists (const std::string& filename) {
  struct stat buffer;   
  return (stat (filename.c_str(), &buffer) == 0); 
}

int GetDirectoryFiles(const std::string& dir,std::vector<std::string> *files) {
  std::string name;

  std::shared_ptr<DIR> directory_ptr(opendir(dir.c_str()), [](DIR* dir){ dir && closedir(dir); });
  struct dirent *dirent_ptr;
  if (!directory_ptr) {
    std::cout << "Error opening : " << dir << std::endl;
    return 1;
  }
 
  while ((dirent_ptr = readdir(directory_ptr.get())) != nullptr) {
    name=std::string(dirent_ptr->d_name);
    if (name.length()>2)
      files->push_back(name);
  }
  return 0;
}


int parseline(std::string line, std::vector<std::string> &result){
    std::stringstream ss(line);
  while(ss.good())
    {
      std::string word;
      getline(ss,word,':');
      result.push_back(word);
    }
  return 0; 
}



int main(int argc, char *argv[]){
  int id;
  //build parameters from file
  Parameters param=Parameters();

  bool eaf = exists ("application_metrics.csv");
  bool eef = exists("execution_metrics.csv");
  bool eoc = exists("occupation.csv");
  
  //Output headers
  std::ofstream cam("application_metrics.csv",std::ofstream::app);
  std::ofstream cem("execution_metrics.csv",std::ofstream::app);
  std::ofstream cocc("occupation.csv",std::ofstream::app);
  //if (!eaf)
   // cam<<"SchedPol \t ContPol \t Em \t S \t alpha \t p \t w \t v \t n \t start \t end"<<std::endl;
  if (!eef){
    cem<<"SchedPol \t ContPol \t Em \t S \t alpha \t b \t makespan \t max_stretch \t mean_stretch";
    cem<<" \t total_compute \t total_io \t total_cont \t total_idle \t vi"<<std::endl;
    //if (!eoc)
    //cocc<<"clock \t executed \t occupation"<<std::endl;
  }
  
  
  //std::cout<<"Generating files using parameter: "<<param.getGen()<<std::endl;

  generator(param);
  
  std::cerr<<"Generation to file completed"<<std::endl;
  //-----------------------------------------------------------------------------------------------------------------------------------------------

  int done=0;
  //auto starting_date= std::chrono::system_clock::now();
  //Print progression
  
  
  //Loading s
  std::vector<std::string> filelist;
  filelist.clear();
  GetDirectoryFiles(param.getDir(),&filelist);
  //std::cout<<"Building applist from files\n";
  for (std::vector<std::string>::iterator fileit = filelist.begin(); fileit != filelist.end(); ++fileit)
    {
      std::string oldloc =("workloads/"+*fileit);
      std::string newloc=("current/"+*fileit);
	
      std::rename(oldloc.c_str(),newloc.c_str());
      
      do {

	auto now = std::chrono::system_clock::now();
	//std::time_t time_now = std::chrono::system_clock::to_time_t(now);
	//std::cout<<std::ctime(&time_now)<<"done: "<<done<<"       /"<<param.numParam()<<" - "<<(done*100)/(param.numParam())<<"%"<<std::endl;
	done++;
	//Parsing file to build apps
	std::string line;
	
	//To define file in options ?
	std::ifstream file ("current/"+*fileit);
	
	std::vector<App*>applist;
	applist.clear();
	std::vector<std::string> app_param;
	id=0;
	//std::cout<<"###########################################"<<std::endl;
	//std::cout<<"p "<<done<<" ";
	std::cout<<"File: "<<*fileit<<std::endl;
	std::cout<<"Parameters: ";
	param.printParam();
	if (file.is_open())
	  {
	    while ( getline (file,line) )
	      {
		app_param.clear();
		parseline(line,app_param);
		applist.push_back(new App(app_param, id,param.getB()));
		++id;
	      }
	    file.close();
	  }
	else
	  std::cout << "Unable to open file\n"; 
	
	std::cerr<<"Applist completed"<<std::endl;
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//Print applist
	//print_applist(applist);
	if(param.getSchedPol()==0){
	  cocc<<"Workload_Pack_maxtime_"<<*fileit<<std::endl;
	  cam<<"Application_Pack_maxtime_"<<*fileit<<std::endl;
  }
  	if(param.getSchedPol()==2){
	  cocc<<"Workload_Pack_chartime_"<<*fileit<<std::endl;
	  cam<<"Application_Pack_chartime_"<<*fileit<<std::endl;
	}
	if(param.getSchedPol()==4){
	  cocc<<"Workload_LS_"<<*fileit<<std::endl;
	  cam<<"Application_LS_"<<*fileit<<std::endl;
	}

	if(param.getSchedPol()==6){
	  cocc<<"Workload_Pack_nosort_"<<*fileit<<std::endl;
	  cam<<"Application_Pack_nosort_"<<*fileit<<std::endl;
	}
	
	cocc<<"clock \t executed \t occupation \t rem_ratio \t rem_abs \t usP \t usB \t t1 \t t2 \t t3 \t t4 \t n \t n1 \t n2 \t n3 \t n4"<<std::endl;
	cam<<"SchedPol \t ContPol \t Em \t S \t alpha \t p \t w \t v \t n \t start \t end \t adv \t eta"<<std::endl;
	
	
	//cam pour tracer les mesures sur les entrées
	
	//Boucles sur les différents paramètres
	//Fonction scheduler of applist including
	std::cerr<<"Starting scheduler"<<std::endl;
	scheduler(&applist,&param);
	std::cerr<<"Scheduler ended, starting controller"<<std::endl;
	controller(&applist,&param,cam,cem,cocc);
	std::cerr<<"Controller ended"<<std::endl;
	//Print ?
	
      }
      while(param.nextParam());
      oldloc=("current/"+*fileit);
      newloc=("completed/"+*fileit);
      std::rename(oldloc.c_str(),newloc.c_str());
    }

  cam.close();
  cem.close();


  return 0;
}
