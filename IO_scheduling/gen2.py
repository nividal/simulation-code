#! /usr/bin/env python3
import math
import sys
import random
import argparse
import string
import os

n_input=1000



"""
On veut un nom,
entre 2 et 10 appli (uniformement)
des temps de calcul
des temps d'I/O qui font entre 10 et 50% de l'exec totale -> 1/10 
entre 10 et 1000 iterations
"""

os.mkdir("in")

random.seed()

for i in range(n_input):
    sys.stdout.write("\r-  {0}  /  {1}  -  ({2}%)   -".format(i,n_input, int(i/n_input*100)))
    n_appli=random.randint(2,15)
    listapp=[]
    #not writing in the loop to sort files
    for a in range(n_appli):
        dur=random.randint(3600,21600)
        app = {}
        app["id"]=''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)])
        app["vol_w"]=  random.randint(10,50)
        app["vol_io"]= int(random.uniform(0.1,1)*app["vol_w"])
        app["n_per"]= int(dur/(app["vol_w"]+app["vol_io"]))  
        app["seed"]=random.random()
        app["release"]=0#random.randint(1,100)
        listapp.append(app) 
    with open("in/{}.txt".format(''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)])),"w") as f:
        for app in listapp:
            f.write(str(app))
            f.write('\n')
    

